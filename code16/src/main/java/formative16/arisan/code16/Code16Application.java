package formative16.arisan.code16;

import java.util.*;
import java.util.regex.Pattern;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Code16Application {
	static public Map<Peserta, Boolean> slot = new HashMap<Peserta,Boolean>(); //global
	static int counter=0;
	static List<Arisan> rekap = new LinkedList<Arisan>();

	public static void main(String[] args) {
		SpringApplication.run(Code16Application.class, args);

		initPeserta();

		System.out.println("\n - - - ARISAN INFORMATIKA '17 - - - -");

		showMainMenu();
	}

	public static void initPeserta() {
		Peserta peserta1 = new Peserta("Ayuna", generateCode() );
		slot.put(peserta1, false);
		Peserta peserta2 = new Peserta("Ikhsan", generateCode() );
		slot.put(peserta2, false);
		Peserta peserta3 = new Peserta("Pipo", generateCode());
		slot.put(peserta3, false);
		Peserta peserta4 = new Peserta("Desha", generateCode());
		slot.put(peserta4, false);
		Peserta peserta5 = new Peserta("Intan", generateCode());
		slot.put(peserta5, false);
		Peserta peserta6 = new Peserta("Reena", generateCode());
		slot.put(peserta6, false);
		Peserta peserta7 = new Peserta("Ve", generateCode());
		slot.put(peserta7, false);
		Peserta peserta8 = new Peserta("Vincent", generateCode());
		slot.put(peserta8, false);
		Peserta peserta9 = new Peserta("Koko", generateCode());
		slot.put(peserta9, false);
		Peserta peserta10 = new Peserta("Primata", generateCode());
		slot.put(peserta10, false);
	}

	public static void showMainMenu() {
		Scanner scan = new Scanner(System.in);
		boolean pilihanValid=false;

		do {
			System.out.println("\n- - - - - -   MAIN MENU  - - - - - -");
			System.out.println("1. Tambah Member");
			System.out.println("2. Mulai Arisan");
			System.out.println("3. Rekap Arisan");
			System.out.println("4. Exit");
			System.out.print("Pilihan Anda : ");
			String pilihan = scan.next();
			pilihanValid = Pattern.compile("[1-4]").matcher(pilihan).matches();
			if(pilihanValid==true) {
				showDetailMenu(scan, pilihan);
			}else {
				System.out.println("Input tidak valid, Coba lagi...");
			}
		}while(pilihanValid==false);

	}

	public static void showDetailMenu(Scanner scan, String input) {
		int pilihan = Integer.parseInt(input);

		switch(pilihan) {
		case 1: menuTambahMember(scan);
		break;

		case 2: menuMulaiArisan(); //init arisan
		break;

		case 3: menuRekap(); //rekap
			break;
		case 4: System.out.println("Bye!"); 
		System.exit(0);
		}
	}


	public static void menuTambahMember(Scanner scan) {
		boolean namaValid =false;

		System.out.println("\n- - - -  - Tambah Member - - - - - -");
		do {
			System.out.print("Nama\t: ");
			String inNama = scan.next();
			namaValid= Pattern.compile("[A-z]{4,25}").matcher(inNama).matches();
			if(namaValid==true) {
				Peserta pesertaBaru = new Peserta(inNama,generateCode());
				slot.put(pesertaBaru, false);
				System.out.println("Tambah Member Berhasil...");
				showMember(slot);
				namaValid=true;
			}else {

				System.out.println("Input tidak valid, Coba lagi...");
				System.out.println("Nama harus terdiri dari minimal 4 Huruf");

			}
		}while(namaValid==false);
		showMainMenu();
	}

	public static void menuMulaiArisan() {
		//generate event arisan
		Peserta updatePemenang;
		Arisan arisanevnt = new Arisan(slot); //slot saat ini
		updatePemenang = arisanevnt.mulaiArisan(counter);
		if(updatePemenang!=null) {
			slot.replace(updatePemenang, true);
			counter++;
			showMember(slot);
			rekap.add(arisanevnt.getRekapArisan());
		}else {
			//do something

		}
		showMainMenu();

	}
	public static void menuRekap() {
		if(!rekap.isEmpty()) {
			System.out.println("\n- - - - - -     REKAP    - - - - - -");
		for(Arisan a : rekap) {
			
			System.out.println("Tanggal\t\t : "+a.getTheEvent());
			System.out.println("Pemenang Arisan : "+a.getTheWinner().getNama());
			System.out.println("- - - - - - - - - - - - - - - - - - -");
		}
		}else {
		    System.out.println("\n- - - - -  REKAP IS EMPTY - - - - - -");	
		}
		showMainMenu();
	}
	public static String generateCode() {
		int codeInt;
		String codeString;
		Peserta p;
		Random random = new Random();
		codeInt = random.nextInt(100);
		codeString = Integer.toString(codeInt);		
		for(Map.Entry<Peserta, Boolean> entry : slot.entrySet()) {
			p = entry.getKey();
			if (codeString.equals(p.getRandomCode())) { //jika code ada yang sama
				codeInt = random.nextInt(100);
				codeString = Integer.toString(codeInt);		
			}
		}
		return codeString;
	}

	public static void showMember(Map<Peserta,Boolean> theSlot) {

		System.out.println("\n- - - - -  Daftar Member   - - - - -");
		System.out.println("|Code\t|Nama\t|Status\t\t|");
		System.out.println("- - - - - -  - - - - - - - - - ");
		for(Map.Entry<Peserta, Boolean> entry : theSlot.entrySet()) {
			String code = entry.getKey().getRandomCode();
			String nama = entry.getKey().getNama();
			String status = getStatus(entry.getValue());
			System.out.println(code+"\t"+nama+"\t"+status);
		}
	}

	public static String getStatus(boolean b) {
		if(b==true) {
			return "  GET";
		}else {
			return "  -";
		}
	}

}
