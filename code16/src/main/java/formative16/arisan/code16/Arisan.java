package formative16.arisan.code16;
import java.time.LocalDate;
import java.util.*;

public class Arisan {
	private Map<Peserta,Boolean> slot = new HashMap<Peserta,Boolean>();
	private List<String> listCode = new LinkedList<String>();
	private final int uangIuran = 150000; //perorang
	private int totalUangIuran = 0; //semua
	private LocalDate theEvent;
	private Peserta theWinner;



	public Arisan (Map<Peserta, Boolean> slot) {
		this.slot = slot;
	}
	

	public Peserta getTheWinner() {
		return theWinner;
	}

	public LocalDate getTheEvent() {
		return theEvent;
	}
	public Map<Peserta, Boolean> getSlot() {
		return slot;
	}

	public int getTotalUangIuran() {
		return totalUangIuran;
	}

	public void setTotalUangIuran(int totalUangIuran) {
		this.totalUangIuran = totalUangIuran;
	}

	public int getUangIuran() {
		return uangIuran;
	}

	public Peserta mulaiArisan(int counter) { //return pemenang
		theWinner = null;
		theEvent = LocalDate.now().plusMonths(counter);
		String winnerCode="";
		//Iuran
		if(!slot.isEmpty()) {
			//hitung total Iuranan berdasar jumlah member	
			for(Map.Entry<Peserta, Boolean> entry : slot.entrySet()) {
				Peserta pesertaTmp = entry.getKey();
				boolean status = entry.getValue();
				totalUangIuran += uangIuran; 
				if(status==false) {
					listCode.add(pesertaTmp.getRandomCode()); //get peserta yg blm menang only
				}
			}

			//kocok
			if(!listCode.isEmpty()) {
				System.out.println("\n- - - - - - MULAI ARISAN - - - - - -");
				Random r = new Random();
				winnerCode = listCode.get(r.nextInt(listCode.size()));
				for(Map.Entry<Peserta, Boolean> entry : slot.entrySet()) {
					Peserta pesertaTmp = entry.getKey();
					if(pesertaTmp.getRandomCode().equals(winnerCode)) {
						theWinner = pesertaTmp;
						slot.replace(theWinner, true); //set sudah menang
						
					}
				}

				//tampil
				System.out.println(".  .  .");
				System.out.println("Tanggal\t\t: "+theEvent);
				System.out.println("Total Iuran : Rp "+totalUangIuran);
				System.out.println(".  .  .");

				System.out.println("- - - - - - - SELAMAT - - - - - - - -");
				System.out.println("Pemenang Arisan Bulan "+  theEvent.getMonth() +" adalah..");
				System.out.println("Nama\t  : "+theWinner.getNama());
				System.out.println("Code\t  : "+theWinner.getRandomCode());
				System.out.println("Total Get : Rp "+this.totalUangIuran);
			
			}else {
				System.out.println("\n- - - - - - ARISAN SELESAI - - - - -");
			}


		}
		return theWinner;

	}
	public Arisan getRekapArisan() {
		return this;
	}
}
