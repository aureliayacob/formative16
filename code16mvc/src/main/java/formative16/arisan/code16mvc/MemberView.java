package formative16.arisan.code16mvc;

import java.time.LocalDate;
import java.util.*;


public class MemberView {

	public void showMainMenu() {
		System.out.println("\n- - - - - -   MAIN MENU  - - - - - -");
		System.out.println("1. Tambah Member");
		System.out.println("2. Mulai Arisan");
		System.out.println("3. Menu Rekap");
		System.out.println("0. Exit");
		System.out.print("Pilihan Anda : ");
	}

	public void showMember(ArrayList<Member> slot) {
		System.out.println("\n- - - - -  Daftar Member   - - - - -");
		System.out.println("|Code\t|Nama\t|Status\t\t|");
		System.out.println("- - - - - -  - - - - - - - - - - - - - ");
	}

	public void mulaiShakeArisan() {
		System.out.println("\n- - - - - - MULAI ARISAN - - - - - -");
	}

	public void winnerFound(LocalDate theEvent, int totalUangIuran, Member theWinner) {
		System.out.println(".  .  .");
		System.out.println("Tanggal\t : "+theEvent);
		System.out.println("Total Iuran : Rp "+totalUangIuran);
		System.out.println(".  .  .");

		System.out.println("- - - - - - - SELAMAT - - - - - - - -");
		System.out.println("Pemenang Arisan Bulan "+  theEvent.getMonth() +" adalah..");
		System.out.println("Nama\t  : "+theWinner.getNama());
		System.out.println("Code\t  : "+theWinner.getRandomCode());
		System.out.println("Total Get : Rp "+totalUangIuran);
	}
	public void arisanDone() {
		System.out.println("\n- - - - - - ARISAN SELESAI - - - - -");
	}
	}
