package formative16.arisan.code16mvc;

import java.time.LocalDate;
import java.util.*;
import java.util.regex.Pattern;

public class MemberController {
	private List<String> toShake = new LinkedList<String>();
	private Member model;
	private Rekap modelRekap = new Rekap();
	private RekapView viewRekap = new RekapView();
	private RekapController controllerRekap = new RekapController(modelRekap,viewRekap);
	private MemberView view;
	private ArrayList<Member> slot = new ArrayList<Member>();
	private ArrayList<Rekap> rekap = new ArrayList<Rekap>();
	private final int uangIuran = 150000; //perorang
	private int totalUangIuran = 0;
	private LocalDate theEvent;
	private Member theWinner;
	private int counter=0;


	public MemberController(Member model, MemberView view) {
		this.model = model;
		this.view = view;
		initSlot();

	}

	public void setMemberName(String name) {
		model.setNama(name);
	}

	public void setMemberRandomCode(String code) {
		model.setRandomCode(code);
	}

	public void setMemberStatus(boolean status) {
		model.setStatus(status);
	}

	public void showMainMenu() {
		Scanner scan = new Scanner(System.in);
		boolean pilihanValid=false;
		do {
			view.showMainMenu();
			String pilihan = scan.next();
			pilihanValid = Pattern.compile("[0-3]").matcher(pilihan).matches();
			if(pilihanValid==true) {
				showDetailMenu(scan, pilihan);
			}else {
				System.out.println("Input tidak valid, Coba lagi...");
			}
		}while(pilihanValid==false);

	}

	public void showDetailMenu(Scanner scan, String input) {
		int pilihan = Integer.parseInt(input);

		switch(pilihan) {
		case 1: menuTambahMember(scan);
		break;
		case 2: menuMulaiArisan(); //init arisan
		break;
		case 3 : menuRekap();
		break;
		case 0: System.out.println("Bye!"); 
		System.exit(0);
		}
	}

	public void menuTambahMember(Scanner scan) {
		boolean namaValid =false;
		System.out.println("\n- - - -  - Tambah Member - - - - - -");
		do {
			System.out.print("Nama\t: ");
			String inNama = scan.next();
			namaValid= Pattern.compile("[A-z]{4,25}").matcher(inNama).matches();
			if(namaValid==true) {
				Member memberBaru = new Member(inNama,generateCode(), false);
				slot.add(memberBaru);
				System.out.println("Tambah Member Berhasil...");
				showMembers();
				namaValid=true;
			}else {
				System.out.println("Input tidak valid, Coba lagi...");
				System.out.println("Nama harus terdiri dari minimal 4 Huruf");
			}
		}while(namaValid==false);
		showMainMenu();
	}

	public void menuMulaiArisan() { //return pemenang
		theEvent = LocalDate.now().plusMonths(counter);
		//Mulai
		if(!slot.isEmpty()) {
			startIuran(); //hitung total Iuranan berdasar jumlah member	
			if(!toShake.isEmpty()) {//shake
				startShake();
				//tampil
				if(theWinner!=null) {
					view.winnerFound(theEvent, totalUangIuran, theWinner);
					rekap.add(new Rekap(theEvent,theWinner));
					//reset
					totalUangIuran=0;
					toShake = new LinkedList<String>();
					
				}
				showMembers();
			}else {
				view.arisanDone();
			}
		}
		showMainMenu();
	}

	public void startIuran() {
		for(Member m : slot) {
			totalUangIuran += uangIuran; 
			if(m.getStatus()==false) {
				toShake.add(m.getRandomCode()); //get peserta yg blm menang only
			}
		}
	}
	public void startShake() {
		String winnerCode="";
		view.mulaiShakeArisan();
		Random r = new Random();
		winnerCode = toShake.get(r.nextInt(toShake.size()));
		for(int i =0; i<slot.size(); i++) {
			if(slot.get(i).getRandomCode().equals(winnerCode)) {
				slot.get(i).setStatus(true);
				theWinner = slot.get(i);
				counter++;

			}
		}
	}

	public void menuRekap() {
		controllerRekap.printRekap(rekap);
		showMainMenu();
	}

	public String generateCode() {
		int codeInt;
		String codeString;
		Member p;
		Random random = new Random();
		codeInt = random.nextInt(100);
		codeString = Integer.toString(codeInt);		
		for(Member m : slot) {
			if (codeString.equals(m.getRandomCode())) { //jika code ada yang sama
				codeInt = random.nextInt(100);
				codeString = Integer.toString(codeInt);		
			}
		}
		return codeString;
	}

	public void showMembers() {
		view.showMember(slot);
		for(Member m : slot) {
			System.out.println(m.getRandomCode()+"\t"+m.getNama()+"\t"+getStatus(m.getStatus()));
		}	
	}
	public static String getStatus(boolean b) {
		if(b==true) {
			return "  GET";
		}else {
			return "  -";
		}
	}
	public ArrayList<Member> initSlot(){
		slot.add(new Member("Ayuna", generateCode(), false));
		slot.add(new Member("Ikhsan", generateCode(), false));
		slot.add(new Member("Pipo", generateCode(), false));
		slot.add(new Member("Desha", generateCode(), false));
		slot.add(new Member("Koko", generateCode(), false));
		return slot;
	}

	public void run() {
		showMainMenu();
	}

}

