package formative16.arisan.code16mvc;

import java.time.LocalDate;

public class Rekap {
	private LocalDate theEvent;
	private Member theWinner;
	
	public Rekap() {
		super();
	}
	public Rekap(LocalDate theEvent, Member theWinner) {
		super();
		this.theEvent = theEvent;
		this.theWinner = theWinner;
	}
	public LocalDate getTheEvent() {
		return theEvent;
	}
	public void setTheEvent(LocalDate theEvent) {
		this.theEvent = theEvent;
	}
	public Member getTheWinner() {
		return theWinner;
	}
	public void setTheWinner(Member theWinner) {
		this.theWinner = theWinner;
	}
	
}
