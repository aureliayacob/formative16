package formative16.arisan.code16mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Code16mvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(Code16mvcApplication.class, args);
		Member model  = new Member();
		MemberView view = new MemberView();
		MemberController controller = new MemberController(model, view);
		controller.run();
	}

}
