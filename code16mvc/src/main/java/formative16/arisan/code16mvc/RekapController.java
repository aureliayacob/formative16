package formative16.arisan.code16mvc;

import java.time.LocalDate;
import java.util.*;

public class RekapController {
	private Rekap model;
	private RekapView view;
	public RekapController(Rekap model, RekapView view) {
		super();
		this.model = model;
		this.view = view;
	}
	public void setEvent(LocalDate date) {
		model.setTheEvent(date);
	}
	public void setWinner(Member member) {
		model.setTheWinner(member);
	}
	public void printRekap(ArrayList<Rekap> rekap) {
		for(Rekap r : rekap) {
			if(r.getTheWinner()!=null) {
				view.rekapHeader();
				view.printRekap(r.getTheEvent(), r.getTheWinner());
			}else {
				view.rekapEmpty();
			}
		}
	}

}
