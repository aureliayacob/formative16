package formative16.arisan.code16mvc;

public class Member {
	private String nama;
	private String randomCode;
	private boolean status;
	
	
	public Member() {
		super();
	}

	public Member(String nama, String randomCode, boolean status) {
		this.nama = nama;
		this.randomCode = randomCode;
		this.status = status;
	}	

	public boolean getStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public void setRandomCode(String randomCode) {
		this.randomCode = randomCode;
	}


	public String getNama() {
		return nama;
	}

	public String getRandomCode() {
		return randomCode;
	}

}
