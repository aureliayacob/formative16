package formative16.arisan.code16mvc;

import java.time.LocalDate;

public class RekapView {
	public void rekapHeader() {
		System.out.println("\n- - - - - -     REKAP    - - - - - -");
	}
	public void rekapEmpty() {
		System.out.println("\n- - - - - - REKAP EMPTY  - - - - - -");
	}
	public void printRekap(LocalDate theEvent, Member theWinner) {
		System.out.println("Tanggal\t\t : "+theEvent);
		System.out.println("Pemenang Arisan : "+theWinner.getNama());
		System.out.println("- - - - - - - - - - - - - - - - - - -");
	}

}
